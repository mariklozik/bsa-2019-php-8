@extends('layouts.app')
@section('content')
    <form method="POST" action="{{ route('products.update', ['product' => $product]) }}">
        @csrf
        @method('PATCH')
        <p>
            <label>
                Name
                <input name="name" value="{{ $product->name }}">
            </label>
        </p>
        <p>
            <label>
                Price
                <input name="price" value="{{ $product->price }}">
            </label>
        </p>
        <button type="submit">Save</button>
    </form>
@endsection