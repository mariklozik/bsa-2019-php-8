@extends('layouts.app')
@section('content')
    @if ( !is_null($product) )
        <table>
            <tr>
                <th>ID</th>
                <th>Name</th>
                <th>Price</th>
                <th>User</th>
            </tr>
            <tr>
                <td>{{ $product->id }}</td>
                <td>{{ $product->name }}</td>
                <td>{{ $product->price }}</td>
                <td>{{ $product->user->name }}</td>
            </tr>
        </table>
    @else
        <p>No products, you can add product</p>
    @endif
@endsection
