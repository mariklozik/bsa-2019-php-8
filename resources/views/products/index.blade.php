@extends('layouts.app')
@section('content')
    @if ( count($products) > 0 )
        <table>
            <tr>
                <th>ID</th>
                <th>Name</th>
                <th>Price</th>
                <th>User</th>
                <th>Action</th>
            </tr>
            @foreach($products as $product)
                <tr>
                    <td>
                        <a href="{{ route('products.show', ['id' => $product->id ]) }}">{{ $product->id }}</a>
                    </td>
                    <td>{{ $product->name }}</td>
                    <td>{{ $product->price }}</td>
                    <td>{{ $product->user->name }}</td>
                    <td>
                        @can('update', $product)
                            <a href="/products/{{ $product->id }}/edit">Edit</a>
                        @endcan
                        @can('delete', $product)
                            <form method="POST" action="/products/{{ $product->id }}">
                                @csrf
                                @method('DELETE')

                                <button type="submit">Delete</button>
                            </form>
                        @endcan
                    </td>
                </tr>
            @endforeach
        </table>
    @else
        <p>No products, you can add product</p>
    @endif

    <p class='add-product'><a href="{{ route('products.create') }}">Add</a></p>
@endsection
