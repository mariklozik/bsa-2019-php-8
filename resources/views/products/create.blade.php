@extends('layouts.app')
@section('content')
    <form method="POST" action="{{ route('products.store') }}">
        @csrf
        <p>
            <label>
                Name
                <input name="name">
            </label>
        </p>
        <p>
            <label>
                Price
                <input name="price">
            </label>
        </p>
        <button type="submit">Save</button>
    </form>
@endsection